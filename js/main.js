const size = 3;

var tableauMorpion = [
  ['', '', ''],
  ['', '', ''],
  ['', '', '']
];

var player = false;

var isOver = false;

function play() {

  if(!isPlayed(this.id) && !isOver) {

    var caseRef = document.getElementById(this.id);

    const getImage = getImageFromPlayer();
    caseRef.innerHTML = '<img '+ 
                            'src='+ getImage + " " +
                            'width="90px"'+
                            'height="90px"'+
                          '>';
    updateTableau(this.id);
    isFinished();
    player = !player;
  }
}

function showMove() {

  if(event.srcElement.id != '' && !isNaN(event.srcElement.id) && !isPlayed(event.srcElement.id) && !isOver) {

    var caseRef = document.getElementById(event.srcElement.id);
    const getImage = getImageFromPlayer();
    caseRef.innerHTML = '<img '+ 
                            'src='+ getImage + " " +
                            'width="90px"'+
                            'height="90px" '+
                            'style="opacity: 0.3" '+
                          '>';
  }
}

function removeMove() {

  if(event.srcElement.id != '' && !isNaN(event.srcElement.id) && !isPlayed(event.srcElement.id) && !isOver) {

    var caseRef = document.getElementById(event.srcElement.id);
    caseRef.innerHTML = '';
  }
}

function getImageFromPlayer() {

  if(player) {

    return "./img/blue_circle.png"
  } else {

    return "./img/red_cross.png"
  }
}

function getSymbolFromPlayer() {

  if(player) {

    return "O"
  } else {

    return "X"
  }
}

function updateTableau(id) {

  tableauMorpion[parseInt(id/size)][id%size] = getSymbolFromPlayer();
}

function isPlayed(id) {

  return tableauMorpion[parseInt(id/size)][id%size] != '';
}

function isFinished() {

  if(checkRows() || checksCol() || checksDiago() || checksReverseDiago()) {

    isOver = true;

    if(player) {

      console.log("blue player won");
    } else {

      console.log("red player won");
    }
  } else if(isEquality()) {

    isOver = true;

    console.log("egalier");
  }

}

function isEquality() {

  for(var i = 0; i < size; i++) {

    for(var j = 0; j < size; j++) {

      if(tableauMorpion[i][j] == '') {

        return false;
      }
    }
  }

  return true;
}

function checkRows() {

  for(var i = 0; i < size; i++) {

    const firstCaracter = tableauMorpion[i][0] == '' ? 'null' : tableauMorpion[i][0];
    var j = 1;
    while(firstCaracter ==  tableauMorpion[i][j]) {
      
      j++;
      if(j == 3) {

        return true;
      }
    }
  }

  return false;
}

function checksCol() {

  for(var i = 0; i < size; i++) {

    const firstCaracter = tableauMorpion[0][i] == '' ? 'null' : tableauMorpion[0][i];
    var j = 1;
    while(firstCaracter ==  tableauMorpion[j][i]) {
      
      j++;
      if(j == 3) {

        return true;
      }
    }
  }

  return false;
}

function checksDiago() {

  const firstCaracter = tableauMorpion[0][0] == '' ? 'null' : tableauMorpion[0][0];

  var j = 1;

  while(firstCaracter ==  tableauMorpion[j][j]) {
    
    j++;
    if(j == 3) {

      return true;
    }
  }

  return false;

}

function checksReverseDiago() {

  const firstCaracter = tableauMorpion[0][2] == '' ? 'null' : tableauMorpion[0][2];

  var j = 1;

  while(firstCaracter == tableauMorpion[j][2 - j]) {
    
    j++;
    if(j == 3) {

      return true;
    }
  }

  return false;

}
  

var tableau = document.getElementsByClassName("cell");

for(var i = 0; i < tableau.length; i++) {

  tableau[i].addEventListener("click", play);
  tableau[i].addEventListener("mouseover", showMove);
  tableau[i].addEventListener("mouseleave", removeMove);
}
